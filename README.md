# 使用方式
## 阿里云申请 OSS 服务使用并获取如下参数
```text
    access-key-id: LTA**************3onU  # 访问身份验证用户标识
    access-key-secret: 4Odnj2**************ScD # 访问身份验证用户密钥
    endpoint: oss-cn-************s.com # oss 服务的访问域名
```
## pom.xml 文件引入如下依赖
```xml
    <dependency>
        <groupId>com.aliyun.oss</groupId>
        <artifactId>aliyun-sdk-oss</artifactId>
        <version>2.5.0</version>
    </dependency>
    <dependency>
        <groupId>com.aliyun</groupId>
        <artifactId>aliyun-java-sdk-core</artifactId>
        <version>4.3.8</version>
    </dependency>
```
## application.yaml 文件引入如下配置
```yaml
aliyunoss:
  enabled: true # 开启阿里云 OSS 上传
  aliyun:
    access-key-id: LTA**************3onU  # 访问身份验证用户标识
    access-key-secret: 4Odnj2**************ScD # 访问身份验证用户密钥
    endpoint: oss-cn-************s.com # oss 服务的访问域名
```

## 启动类加入如下注解
```java
@EnableAliyunOSS
```
## 测试

### 服务端上传
```java
    @Autowired
    private Storage storage;

    // 后端上传
    @Test
    public void ossTest() throws Exception {
       File file = new File("C:\\Users\\*******\\5555533333333.png");
       InputStream inputStream = new FileInputStream(file);
       storage.uploadFile("***", "2020/12/14/", "55555333333332222333.png", inputStream, MimeTypeUtils.IMAGE_PNG_VALUE);
       String fileUrl = storage.getFileUrl("***", "2020/12/14/", "55555333333332222333.png");
       log.info(fileUrl);
    }
```

### 服务端授权+浏览器上传
1. 服务端授权
```java
OssPolicy ossPolicy = storage.getOssPolicy("alusiaor", "2020/12/14/", 10, 300);
```
2. 授权结果
```json
{
	"signature": "rIpDxy*********haaHG4g=",
	"success_action_status": 200,
	"host": "https://********.oss-cn-beijing.aliyuncs.com",
	"ossaccessKeyId": "LTAI4GG***********oJ4G3onU",
	"key": "2020/12/14/",
	"url": "https://********.oss-cn-beijing.aliyuncs.com/2020/12/14/",
	"policy": "eyJleHBpcmF0aW9uIj*********************************NC8iXV19"
}
```
3. 浏览器上传
```js

// 请求示例（jQuery 版）
var fileName="99999999988888888.png";
var form = new FormData();
form.append("success_action_status"," 授权结果参数【success_action_status】");
form.append("ossaccessKeyId","授权结果参数【ossaccessKeyId】");
form.append("policy", "授权结果参数【policy】");
form.append("signature", "授权结果参数【signature】");
form.append("key", "授权结果参数【key】"+fileName);
form.append("file", fileInput.files[0], "待上传文件");

var settings = {
  "url": "https://*******.oss-cn-beijing.aliyuncs.com",
  "method": "POST",
  "timeout": 0,
  "processData": false,
  "mimeType": "multipart/form-data",
  "contentType": false,
  "data": form
};

$.ajax(settings).done(function (response) {
if(response.status===200){
  console.log("授权结果参数【key】"+fileName);
}

});

// 请求示例（ NodeJs - Axios 版）

var fileName="99999999988888888.png";
var axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
var data = new FormData();
data.append('success_action_status', '200');
data.append('ossaccessKeyId', 'LTAI4GGE*****coJ4G3onU');
data.append('policy', 'eyJleHBpcmF0aW9uIjo***************NC8iXV19');
data.append('signature', 'rIpDxyTDkbp**********HG4g=');
data.append("key", "授权结果参数【key】"+fileName);
data.append('file', fs.createReadStream("待上传文件"));

var config = {
  method: 'post',
  url: 'https://*****.oss-cn-beijing.aliyuncs.com',
  headers: { 
    ...data.getHeaders()
  },
  data : data
};

axios(config)
.then(function (response) {
 if(response.status===200){
   console.log("授权结果参数【key】"+fileName);
 }
})
.catch(function (error) {
  console.log(error);
});


```

4. 文件访问URL
```text
授权结果参数【url】 + fileName
```