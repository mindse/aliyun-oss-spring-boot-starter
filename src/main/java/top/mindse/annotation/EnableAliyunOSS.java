package top.mindse.annotation;


import org.springframework.context.annotation.Import;
import top.mindse.autoconfigure.OssAutoConfiguration;

import java.lang.annotation.*;

/**
 * 导入  aliyun-oss-spring-boot-starter 配置类
 *
 * @author mindse
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(OssAutoConfiguration.class)
public @interface EnableAliyunOSS {
}