package top.mindse.model;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * aliyun-oss 配置类
 *
 * @author mindse
 */
@ConfigurationProperties(prefix = "aliyunoss")
@Data
public class OSSProperties {
    private boolean enabled;

    private Aliyun aliyun;

    @Data
    public static class Aliyun {
        /**
         * oss 服务的访问域名
         */
        private String endpoint;
        /**
         * 访问身份验证用户标识
         */
        private String accessKeyId;
        /**
         * 访问身份验证用户密钥
         */
        private String accessKeySecret;
    }


}
