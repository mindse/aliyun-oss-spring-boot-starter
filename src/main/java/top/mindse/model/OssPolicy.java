package top.mindse.model;

import lombok.Data;

/**
 * 获取OSS上传文件授权返回结果
 *
 * @author mindse
 */

@Data
public class OssPolicy {

    /**
     * 访问身份验证中用到用户标识
     */
    private String ossaccessKeyId;

    /**
     * 用户表单上传的策略,经过base64编码过的字符串"
     */
    private String policy;

    /**
     * 对policy签名后的字符串
     */
    private String signature;

    /**
     * 上传文件夹路径前缀
     */
    private String key;

    /**
     * oss对外服务的访问域名
     */
    private String host;

    /**
     * 文件访问地址
     */
    private String url;

    /**
     * 状态码
     */
    private Integer success_action_status;
}
