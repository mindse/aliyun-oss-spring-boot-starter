package top.mindse.autoconfigure;


import com.aliyun.oss.OSSClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.mindse.model.OSSProperties;
import top.mindse.oss.AliyunStorage;
import top.mindse.oss.Storage;

/**
 * aliyun-oss-spring-boot-starter 自动配置类
 *
 * @author mindse
 */
@Configuration
@EnableConfigurationProperties(OSSProperties.class)
public class OssAutoConfiguration {


    @Bean
    @ConditionalOnProperty(prefix = "aliyunoss", name = "enabled", havingValue = "true")
    @ConditionalOnClass(OSSClient.class)
    public Storage aliyunOSS(OSSProperties ossProperties) {
        OSSProperties.Aliyun aliyun = ossProperties.getAliyun();
        OSSClient ossClient = new OSSClient(aliyun.getEndpoint(), aliyun.getAccessKeyId(), aliyun.getAccessKeySecret());
        return new AliyunStorage(ossClient);
    }
}
