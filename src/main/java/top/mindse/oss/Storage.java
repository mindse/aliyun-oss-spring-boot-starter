package top.mindse.oss;

import top.mindse.model.OssPolicy;

import java.io.InputStream;

/**
 * OSS 上传逻辑类
 *
 * @author mindse
 */
public interface Storage {


    /**
     * 文件上传
     *
     * @param bucketName  存储空间名称
     * @param storagePath 存储路径
     * @param fileName    文件名称
     * @param inputStream 文件输入流
     * @param contentType 文件格式
     */
    void uploadFile(String bucketName, String storagePath, String fileName, InputStream inputStream, String contentType);

    /**
     * 获取文件
     *
     * @param bucketName  存储空间名称
     * @param storagePath 存储路径
     * @param fileName    文件名称
     * @return 文件输入流
     */
    InputStream getFile(String bucketName, String storagePath, String fileName);

    /**
     * 获取文件路径
     *
     * @param bucketName  存储空间名称
     * @param storagePath 存储路径
     * @param fileName    文件名称
     * @return 文件存储路径
     */
    String getFileUrl(String bucketName, String storagePath, String fileName);

    /**
     * 删除文件
     *
     * @param bucketName  存储空间名称
     * @param storagePath 存储路径
     * @param fileName    文件名称
     */
    void removeFile(String bucketName, String storagePath, String fileName);

    /**
     * oss上传策略生成
     *
     * @param bucketName   存储空间名称
     * @param storagePath  存储路径
     * @param maxSize      上传文件最大存储空间
     * @param policeExpire 签名有效期
     * @return OssPolicy
     */
    OssPolicy getOssPolicy(String bucketName, String storagePath, int maxSize, int policeExpire);
}
